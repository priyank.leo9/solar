$(document).on('ready', function () {

    /*=================================
   Sticky Header Starts
   =================================*/
    var vHeight = $(window).height() * 0.8;

    function fixedHeader() {
        var sticky = $('header'),
            scroll = $(window).scrollTop();

        if (scroll >= 10) sticky.addClass('fixHeader');
        else sticky.removeClass('fixHeader');


        var gotoTop = $('.gotoTop');
        if (scroll >= vHeight) gotoTop.fadeIn();
        else gotoTop.fadeOut();
    };

    $(window).scroll(function (e) {
        fixedHeader();
    });
    fixedHeader();
    /* Sticky Header Ends */

    $('#header').load('header.html', function () {
        fixedHeader();
        sidemenu();
    });
    $('#footer').load('footer.html');

    $('[data-toggle="tooltip"]').tooltip();


    /* menu */
    function sidemenu() {
        $(".side-menu").click(function () {
            console.log(1);
            $('.mobile-menu').addClass('mobile-menu-in');
            $('.overlay').addClass('overlay-in');
        });

        $(".close-btn, .overlay").click(function () {
            $('.overlay').removeClass('overlay-in');
            $('.mobile-menu').removeClass('mobile-menu-in');
        });
    }
    /* menu ends */


    /* Form Feild Functionality */
    $(document).on('input', '.form-field', function () {
        if ($(this).val().length > 0) {
            $(this).addClass('field--not-empty');
        } else {
            $(this).removeClass('field--not-empty');
        }
    });
    /* // Form Feild Functionality */

    /* Password View */
    $(document).on('click', '.view-pass', function () {

        inp = $(this).parents('.form-group').find('input');
        type = inp.attr('type');

        if (type == 'password') {
            inp.attr('type', 'text');
            $(this).removeClass('icon-hide').addClass('icon-seen');
        } else {
            inp.attr('type', 'password');
            $(this).removeClass('icon-seen').addClass('icon-hide');
        }
    });
    /* // Password View */

    /* scroll to top start */
    $('.gotoTop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;

    });

    /* scroll to top end */

    $(document).on('click', '.faqAcc', function (e) {
        // console.log($(this).hasClass('active'));
        if (!$(this).hasClass('active')) {
            $(".faqpara").slideUp();
            $('.faqAcc').removeClass('active');
            $(this).addClass('active');
            $(this).find(".faqpara").slideToggle();
        } else {
            $(".faqpara").slideUp();
            $('.faqAcc').removeClass('active');
        }
        // return
        // e.stopPropogation();
    })


    if ($(".wow").length > 0) {
        var wow = new WOW({
            // offset: 0.5,
            mobile: false
        });
        wow.init();
    }



    $.validator.addMethod("laxEmail", function (value, element) {
        return this.optional(element) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(value);
    }, 'Please enter a valid email address.');

    $(document).on('click', '.nav-item', function () {
        $('.enquiryFormRes').hide();
    });

    var ruleLists = {
        'name': {
            required: true,
            minlength: 2,
        },
        'mob_no': {
            required: true,
            minlength: 10,
            maxlength: 10,
        },
        'email': {
            required: true,
            laxEmail: true
        },
        'pin': {
            required: true,
            minlength: 6,
            maxlength: 6,
        },
        'bill': {
            required: true,
        },
        'c_name': {
            required: true,
        },
        'state': {
            required: true,
        },
        'city': {
            required: true,
        },
        'profession': {
            required: true,
        },
    };
    var messageList = {
        'name': {
            required: "Your Name is required",
            minlength: "Name must be at least 2 characters",
        },
        'mob_no': {
            required: "Mobile Number is required",
            minlength: "Mobile Number must be at least 10 characters",
            maxlength: "Mobile Number must be 10 characters",
        },
        'email': {
            required: "Email Address is required",
            laxEmail: "Please enter a valid email address.",
        },
        'pin': {
            required: "Pin is required",
            minlength: "Pin must be at least 6 characters",
            maxlength: "Pin must be at least 6 characters",
        },
        'bill': {
            required: "Bill is required",
        },
        'c_name': {
            required: "Company Name is required",
        },
        'state': {
            required: "State is required",
        },
        'city': {
            required: "City is required",
        },
        'profession': {
            required: "Profession is required",
        },
    };

    function invalidHandlerFn(event, validator) {
        // 'this' refers to the form
        // var errors = validator.numberOfInvalids();
        var errors = '';
        // console.log(validator);
        if (validator.errorList.length > 0) {
            for (x = 0; x < validator.errorList.length; x++) {
                // console.log(validator.errorList[x].message);
                // errors += validator.errorList[x].message;

                /* Code Starts */
                let obj = {
                    'event': 'errorMessage',
                    'Error Message': validator.errorList[x].message,
                    'Error URL': window.location.href
                };
                console.log(obj);
                // window.dataLayer = window.dataLayer || [];
                // window.dataLayer.push(obj);
                /* Code Ends */

            }
        }
        // console.log(errors);


    }

  


    var validateObj = {
        highlight: function (element) {
            jQuery(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        rules: ruleLists,
        messages: messageList,
        invalidHandler: function (event, validator) {
            invalidHandlerFn(event, validator);
        },
        submitHandler: function (form) {
            sendForm(form);
        }
    };

    if ($("#enquiryForm").length > 0) {
        // $("#enquiryForm").attr("autocomplete", "off");
        $("#enquiryForm").validate(validateObj);
    }
    if ($("#enquiryForm1").length > 0) {
        // $("#enquiryForm1").attr("autocomplete", "off");
        $("#enquiryForm1").validate(validateObj);
    }

})