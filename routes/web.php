<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@index')->name('index');
Route::get('/commercial','PagesController@commercial')->name('commercial');
Route::get('/residential','PagesController@residential')->name('residential');
Route::get('/about-us','PagesController@about')->name('about');
Route::get('/contact-us','PagesController@contact')->name('contact');
Route::get('/go-solar','PagesController@solar')->name('solar');
Route::get('/solar-pro','PagesController@solarPro')->name('solar_pro');
Route::post('/submit-form','PagesController@submitform')->name('submit');
Route::post('/export','PagesController@exportToExcel')->name('exportToExcel');

Route::get('/storage-link',function(){
   \Artisan::call('storage:link');
   return "done";
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
