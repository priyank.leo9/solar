<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['name','email','mob_no','comment'];
	

	public function getCeatedAtAttribute()
	{
		return date('Y-m-d',strtotime($this->created_at));
	}
}

