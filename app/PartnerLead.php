<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerLead extends Model
{
    protected $fillable = ['name','email','mob_no','state','city','profession'];

    public function getCeatedAtAttribute()
	{
		return date('Y-m-d',strtotime($this->created_at));
	}
}
