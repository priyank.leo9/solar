<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Home;
use App\Commercial;
use App\Residential;
use App\Faq;
use App\FaqTypeOne;
use App\FaqTypeTwo;
use App\FaqTypeThree;
use App\AboutCompany;
use App\Vision;
use App\Team;
use App\ContactInfo;
use App\Solar;
use App\SolarPro;
use App\WhomToSell;
use App\Benefit;
use App\HowItWork;
use App\Partner;
use App\FaqPro;
use App\FaqProTypeOne;
use App\FaqProTypeTwo;
use App\FaqProTypeThree;
use Illuminate\Support\Facades\Validator;
use App\CommercialLead;
use App\ResidentialLead;
use App\Message;
use App\State;
use App\Profession;
use App\PartnerLead;
use Illuminate\Support\Facades\Mail;
use Excel;
use App\Exports\MessageExport;
use App\Exports\SolarExport;
use App\Exports\CommercialExport;
use App\Exports\ResidentialExport;

class PagesController extends Controller
{
    public function index()
    {
    	$home = Home::first();
        // return $home->bg_img;
    	return view('index',compact('home'));
    }

    public function commercial()
    {
    	$commercial = Commercial::first();

    	return view('commercial',compact(['commercial']));
    }

    public function residential()
    {
    	$commercial = Residential::first();
    	$faq = Faq::first();
    	$ones = FaqTypeOne::get();
    	$twos = FaqTypeTwo::get();
    	$threes = FaqTypeThree::get();

    	return view('residential',compact(['commercial','faq','ones','twos','threes']));
    }

    public function about()
    {
    	$about = AboutCompany::first();
    	$visions = Vision::get();
    	$teams = Team::get();
    	return view('about',compact('about','visions','teams'));
    }

    public function contact()
    {
    	$about = ContactInfo::first();

    	return view('contact',compact('about'));
    }

    public function solar()
    {
    	$about = Solar::first();
    	$faq = Faq::first();
    	$ones = FaqTypeOne::get();
    	$twos = FaqTypeTwo::get();
    	$threes = FaqTypeThree::get();

    	return view('go_solar',compact('about','faq','ones','twos','threes'));
    }

    public function solarPro()
    {
    	$commercial = SolarPro::first();
    	$sells = WhomToSell::get();
    	$benefits = Benefit::get();
    	$works = HowItWork::get();
    	$partners = Partner::get();
    	$faq = FaqPro::first();
    	$ones = FaqProTypeOne::get();
    	$twos = FaqProTypeTwo::get();
    	$threes = FaqProTypeThree::get();
    	$states = State::orderBy('state')->get();
    	$professions = Profession::orderBy('profession')->get();

    	return view('solar_pro',compact('commercial','sells','benefits','works','partners','faq','ones','twos','threes','states','professions'));
    }

    public function submitform(Request $req)
    {
    	if($req->type == 'commercial')
    	{
    		$validatedData = Validator::make($req->all(), [
							    'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
							    'c_name' => 'required|regex:/^[\pL\s\-\&\0-9]+$/u|max:255',
							    'mob_no' => 'required',
							    'pin' => 'required|numeric',
							    'bill' => 'required|numeric',
							])->validate();

    		$newLead = CommercialLead::create($validatedData);
          
    		if($newLead)
    		{
                $validatedData['type'] = 'commercial';
                $data = array('data' => $validatedData);

                Mail::send('email', $data, function ($message) {
                    $message->from('commercial@solarsquare.in', 'Solar Square');
                    $message->sender('commercial@solarsquare.in', 'Solar Square');
                
                    $message->to('commercial@solarsquare.in', 'Solar Square');
                
                    $message->subject('Solar Square Commercial Inquiry');
                });

               
                $curl_url = 'http://solarsquare.octabees.com/api/WebEnquiry/SaveWebEnquiry';

                $data1 = array('ContactName' => $validatedData['name'],
                                'ContactNo' => $validatedData['mob_no'],
								'CompanyName' => $validatedData['c_name'],
								'MobileNumber' => $validatedData['mob_no'],
								'CustomerCategory' => 'Commercial',
								'CustomerSubCategory' => '',
                                'ProductLine' => 'Solar EPC',
                                'Country' => 'India',
                                'State' => '',
                                'City' => '',
								'Address1' => '',
								'PinCode' => $validatedData['pin'],
								'BranchCode' => "B001",
								'AdditionalFieldAnswer' => [array('AdditionalFieldName' => 'Electricity Bill','answer' => $validatedData['bill'])]
                                ) ;

				$curl_post_fields = json_encode($data1);
				
				// return $curl_post_fields ;
                $header = array(
                'Accept: application/json',
                'Content-Type: application/json',
                
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $curl_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_fields);
                $result = curl_exec($ch);
                $task_status = curl_getinfo($ch,CURLINFO_RESPONSE_CODE);
                $err = curl_error($ch);
                curl_close($ch);
                
				// return response()->json(json_decode($result));

    			$code = 200;
        		$responseData = 'Enquiry sent Successfully';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    		else
    		{
    			$code = 500;
        		$responseData = 'Whoops.!Server under maintainance, try again later.';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}

    	}
    	elseif($req->type == 'residential')
    	{
    		$validatedData = Validator::make($req->all(), [
							    'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
							    'email' => 'required|regex:/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/u|max:255',
							    'mob_no' => 'required',
							    'pin' => 'required|numeric',
							    'bill' => 'required|numeric',
							])->validate();

    		$newLead = ResidentialLead::create($validatedData);
    		if($newLead)
    		{
                $validatedData['type'] = 'residential';
                $data = array('data' => $validatedData);

                Mail::send('email', $data, function ($message) {
                    $message->from('residential@solarsquare.in', 'Solar Square');
                    $message->sender('residential@solarsquare.in', 'Solar Square');
                
                    $message->to('residential@solarsquare.in', 'Solar Square');
                
                    $message->subject('Solar Square Reidential Inquiry');
				});
				
				$curl_url = 'http://solarsquare.octabees.com/api/WebEnquiry/SaveWebEnquiry';

                $data1 = array('ContactName' => $validatedData['name'],
				'ContactNo' => $validatedData['mob_no'],
				'CompanyName' => '',
				'MobileNumber' => $validatedData['mob_no'],
				'CustomerCategory' => 'Residential',
				'CustomerSubCategory' => '',
				'ProductLine' => 'Solar EPC',
				'Country' => 'India',
				'State' => '',
				'City' => '',
				'Address1' => '',
				'PinCode' => $validatedData['pin'],
				'BranchCode' => "B001",
				'AdditionalFieldAnswer' => [array('AdditionalFieldName' => 'Electricity Bill','answer' => $validatedData['bill'])]
                                ) ;

				$curl_post_fields = json_encode($data1);
				
				// return $curl_post_fields ;
                $header = array(
                'Accept: application/json',
                'Content-Type: application/json',
                
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $curl_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_fields);
                $result = curl_exec($ch);
                $task_status = curl_getinfo($ch,CURLINFO_RESPONSE_CODE);
                $err = curl_error($ch);
                curl_close($ch);
                
             
				// return response()->json(json_decode($result));
    			$code = 200;
        		$responseData = 'Enquiry sent Successfully';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    		else
    		{
    			$code = 500;
        		$responseData = 'Whoops.!Server under maintainance, try again later.';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    	}
    	elseif($req->type == 'contact_form')
    	{
    		// return $req->all();
    		$validatedData = Validator::make($req->all(), [
							    'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
							    'email' => 'required|regex:/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/u|max:255',
							    'mob_no' => 'required',
							    'comment' => 'regex:/^[\pL\s\-\&]+$/u',
							])->validate();

    		$newLead = Message::create($validatedData);
    		if($newLead)
    		{
                $validatedData['type'] = 'contact_form';
                $data = array('data' => $validatedData);

                Mail::send('email', $data, function ($message) {
                    $message->from('info@solarsquare.in', 'Solar Square');
                    $message->sender('info@solarsquare.in', 'Solar Square');
                
                    $message->to('info@solarsquare.in', 'Solar Square');
                
                    $message->subject('Solar Square Website Inquiry');
                });

    			$code = 200;
        		$responseData = 'Message sent Successfully';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    		else
    		{
    			$code = 500;
        		$responseData = 'Whoops.!Server under maintainance, try again later.';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    	}
    	elseif($req->type == 'solar_pro')
    	{
    		// return $req->all();
    		$validatedData = Validator::make($req->all(), [
							    'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
							    'email' => 'required|regex:/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/u|max:255',
							    'mob_no' => 'required',
							    'state' => 'required|regex:/^[\pL\s]/u',
							    'city' => 'required|regex:/^[\pL\s]/u',
							    'profession' => 'required|regex:/^[\pL\s]/u',
							])->validate();

    		$newLead = PartnerLead::create($validatedData);
    		if($newLead)
    		{
                $validatedData['type'] = 'solar_pro';
                $data = array('data' => $validatedData);

                Mail::send('email', $data, function ($message) {
                    $message->from('info@solarsquare.in', 'Solar Square');
                    $message->sender('info@solarsquare.in', 'Solar Square');
                
                    $message->to('info@solarsquare.in', 'Solar Square');
                
                    $message->subject('Solar Square to Become A Partner Inquiry');
                });

    			$code = 200;
        		$responseData = 'Inquiry sent Successfully';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    		else
    		{
    			$code = 500;
        		$responseData = 'Whoops.!Server under maintainance, try again later.';
        		$response = array("code" => $code, "message" => $responseData);
        		echo json_encode($response);
    		}
    	}
    	else
    	{
    		$code = 500;
    		$responseData = 'Whoops.!Server under maintainance, try again later.';
    		$response = array("code" => $code, "message" => $responseData);
    		echo json_encode($response);
    	}
    }

    public function exportToExcel(Request $request)
    {
        // $model = 'App\\'.$request->model;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if($request->model == 'Message')
        {
            return Excel::download(new MessageExport($from_date,$to_date),'Messages.xlsx');
        }
        elseif( $request->model == 'solar_pro' )
        {
            return Excel::download(new SolarExport($from_date,$to_date),'SolarPro.xlsx');
        }
        elseif( $request->model == 'commercial' )
        {
            return Excel::download(new CommercialExport($from_date,$to_date),'Commercial.xlsx');
        }
        elseif( $request->model == 'residential' )
        {
            return Excel::download(new ResidentialExport($from_date,$to_date),'Residential.xlsx');
        }
        // return $data_arr;
    }
}
