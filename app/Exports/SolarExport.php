<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\PartnerLead;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SolarExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    public function collection()
    {
        return PartnerLead::whereBetween('created_at',[$this->from_date,$this->to_date])->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'Name',
            'Email',
            'Contact',
            'State',
            'City',
            'Profession',
            'Created At',
            'Updated At'
        ];
    }
}
