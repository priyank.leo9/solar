<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResidentialLead extends Model
{
    protected $fillable = ['name','email','mob_no','pin','bill'];

    public function getCeatedAtAttribute()
	{
		return date('Y-m-d',strtotime($this->created_at));
	}
}
