<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CommercialLead extends Model
{
    protected $fillable = ['name','c_name','mob_no','pin','bill'];
}
