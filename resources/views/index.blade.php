@extends('layout.layout')



        @section('title'){{ $home->meta_title }}@endsection

        

        @section('seo')

        <!-- Title -->

        <meta itemprop="name" content="{{ $home->meta_title }}" />

        <meta name="twitter:title" content="{{ $home->meta_title }}" />

        <meta property="og:title" content="{{ $home->meta_title }}" />



        <!-- Description -->

        <meta name="description" content="{{ $home->meta_description }}" />

        <meta itemprop="description" content="{{ $home->meta_description }}" />

        <meta name="twitter:description" content="{{ $home->meta_description }}" />

        <meta property="og:description" content="{{ $home->meta_description }}" />



        <!-- keywords -->

        <meta name="keywords" content="{{ $home->meta_keywords }}">


        <link rel="canonical" href="{{ $home->canonical }}" />
        <meta property="og:url" content="{{ $home->canonical }}" />
        

        @endsection

         @section('page-level-css')
         @php
        $ban_image = str_replace('\\', '/', $home->bg_img);
        $mob_image = str_replace('\\', '/', $home->mob_image);
      
        @endphp
         <style>
         .HomeBannerMain {
                background: url({{ asset('storage/'.$ban_image) }}) no-repeat center center / cover;
            }
            
            @media all and (max-width:768px) {
            
                .HomeBannerMain {
                    background-image: url({{ asset('storage/'.$mob_image) }});
                }
            }
            </style>
            
         @endsection



        @section('tag')

        <h1>{{ $home->meta_title }}</h1>

        @endsection

        

        @section('content')

        <!-- Header Starts -->

        

        <!-- Header Ends -->

        <!-- Main Container Starts -->

        <div class="mainContainer">



            <div class="HomeBannerMain"></div>

            <div class="homeBan p0">

                <div class="container">

                    <!-- Banner content start -->

                    <div class="commCont">

                        <div >

                            <h2 class="banHdn">{{ $home->title }} <br>{{ $home->second_line_title }} </h2>

                            <p class="banTitle">{{ $home->subtitle }}</p>

                        </div>

                        <div class="homebtn " >

                            <a class="butn" href="{{ route('commercial') }}"><span>Commercial</span></a>

                            <a class="butn" href="{{ route('residential') }}"><span>Residential</span></a>

                        </div>

                    </div>

                    <!-- Banner content end -->

                </div>

            </div>

            <!-- end -->



        </div>

        <!-- Main Container Ends -->

        @endsection

        @section('page-level-js')

        @endsection

       

   

