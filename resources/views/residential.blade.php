@extends('layout.layout')

       @section('title'){{ $commercial->meta_title }}@endsection

        @section('seo')

        <!-- Title -->

        <meta itemprop="name" content="{{ $commercial->meta_title }}" />

        <meta name="twitter:title" content="{{ $commercial->meta_title }}" />

        <meta property="og:title" content="{{ $commercial->meta_title }}" />



        <!-- Description -->

        <meta name="description" content="{{ $commercial->meta_description }}" />

        <meta itemprop="description" content="{{ $commercial->meta_description }}" />

        <meta name="twitter:description" content="{{ $commercial->meta_description }}" />

        <meta property="og:description" content="{{ $commercial->meta_description }}" />



        <!-- keywords -->

        <meta name="keywords" content="{{ $commercial->meta_keywords }}">


        <link rel="canonical" href="{{ $commercial->canonical }}" />
        <meta property="og:url" content="{{ $commercial->canonical }}" />
        

        @endsection

    @section('page-level-css')

    <style>

        .mainBan .swiper-wrapper{

            height: 100%;

        }

        .swiper-wrapper{

            height: auto;

        }

    </style>

    @endsection



        @section('tag')

        <h1>{{ $commercial->meta_title }}</h1>

        @endsection

     @section('content')   

        <!-- go to top -->

        <div class="gotoTop">

            <i class="icon-up-arrow"></i>

        </div>

        <!-- go to top end -->



    <!-- Main Container Starts -->

    <div class="mainContainer">

      <!-- Bnaner start -->

      <div class="swiper-container mainBan commBan-swiper">

                <!-- Additional required wrapper -->

                <div class="swiper-wrapper">

                    <!-- Slides -->

                    <div class="swiper-slide">

                        <div class="banImg leftBan">

                            <img src="{{ asset('storage/'.$commercial->slider_1) }}" alt="{{ $commercial->alt_tag1 }}" name="{{ $commercial->image_name }}">

                            <!-- start -->

                            <div class="container">

                                <div class="commCont wow fadeIn" data-wow-duration="0.5s">

                                    <h2 class="banHdn">{{ $commercial->title1 }}<br />

                                        {{ $commercial->title1_line2 }}</h2>

                                    <p class="banTitle">{{ $commercial->subtitle1 }}</p>

                                </div>

                            </div>

                            <!-- end -->

                        </div>

                    </div>

                    <div class="swiper-slide">

                        <div class="banImg leftBan">

                            <img src="{{ asset('storage/'.$commercial->slider_2) }}" alt="{{ $commercial->alt_tag2 }}" name="{{ $commercial->image_name2 }}">

                            <!-- start -->

                            <div class="container">

                                <div class="commCont wow fadeIn" data-wow-duration="0.5s">

                                    <h2 class="banHdn">{{ $commercial->title2 }}<br />

                                        {{ $commercial->title2_line2 }}</h2>

                                    <p class="banTitle">{{ $commercial->subtitle2 }}</p>

                                </div>

                            </div>

                            <!-- end -->

                        </div>

                    </div>

                    <div class="swiper-slide">

                        <div class="banImg leftBan">

                            <img src="{{asset('storage/'.$commercial->slider_3)}}" alt="{{ $commercial->alt_tag3 }}" name="{{ $commercial->image_name3 }}">

                            <!-- start -->

                            <div class="container">

                                <div class="commCont wow fadeIn" data-wow-duration="0.5s">

                                    <h2 class="banHdn">{{ $commercial->title3 }}<br />

                                        {{ $commercial->title3_line2 }}</h2>

                                    <p class="banTitle">{{ $commercial->subtitle3 }}</p>

                                </div>

                            </div>

                            <!-- end -->

                        </div>

                    </div>

                </div>

                <!-- Add Pagination -->

                <div class="swiper-pagination banPagi"></div>

            </div>

      <!-- Bnaner end -->



      <!-- form start -->

      @include('partials.lead')

      <!-- form end -->

      <!-- Credentials Starts -->

      @include('partials.credentials')

      <!-- Credentials Ends -->



      <!-- Why chose SolarSquare? Starts -->

      @include('partials.why_choose_us')

      <!-- Why chose SolarSquare? Ends -->

      <!-- Customer Testimonials Starts -->

      @include('partials.testimonials')

      <!-- Customer Testimonials Ends -->



      <!-- faq start -->

      <div class="comm-section pb-0 mb-800-50">

        <div class="energyRow revs--energyRow">

          <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

            <!-- start -->

            <div class="solarEng">

              <img src="{{ asset('storage/'.$faq->image) }}" alt="{{ $faq->alt_tag }}" name="{{ $faq->img_name }}" />

            </div>

            <!-- end -->

          </div>

          <div class="energyRight wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">

            <!-- start -->

            <div class="hdn-section left">

              <h5>{{ $faq->heading }}</h5>

            </div>

            <!-- end -->



            <div class="commTab commTab2">

              <ul class="nav nav-pills">

                <li class="nav-item">

                  <a class="nav-link active" data-toggle="pill" href="#faq1">{{ $faq->type1_text }}</a>

                </li>

                <li class="nav-item">

                  <a class="nav-link" data-toggle="pill" href="#faq2">{{ $faq->type2_text }}</a>

                </li>

                <li class="nav-item">

                  <a class="nav-link" data-toggle="pill" href="#faq3">{{ $faq->type3_text }}</a>

                </li>

              </ul>

            </div>



            <!-- Tab panes -->

            <div class="tab-content">

              <div class="tab-pane active" id="faq1">

                <div class="faqacc-box">

                  <!-- start -->

                  @php

                    $i = 1;

                  @endphp

                  @foreach( $ones as $one )

                  <div class="faqAcc">

                    <div class="faqQus">

                      <h4>{{ $one->ques }}</h4>

                    </div>

                    <div class="faqpara">

                      

                        {!! $one->ans !!}

                     

                    </div>

                  </div>

                  <!-- end -->

                  @if($i++ == 4) @break @endif

                  @endforeach

                  

                </div>

              </div>

              @php

                    $i = 1;

                  @endphp

              <div class="tab-pane" id="faq2">

                <div class="faqacc-box">

                  @foreach( $twos as $two )

                  <!-- start -->

                  <div class="faqAcc">

                    <div class="faqQus">

                      <h4>{{ $two->ques }}</h4>

                    </div>

                    <div class="faqpara">

                      

                        {!! $two->ans !!}

                      

                    </div>

                  </div>

                  <!-- end -->

                  @if($i++ == 4) @break @endif

                  @endforeach

                 

                </div>

              </div>

               @php

                    $i = 1;

                  @endphp

              <div class="tab-pane" id="faq3">

                <div class="faqacc-box">

                  <!-- start -->

                  @foreach( $threes as $three )

                  <div class="faqAcc">

                    <div class="faqQus">

                      <h4>{{ $three->ques }}</h4>

                    </div>

                    <div class="faqpara">

                      

                        {!! $three->ans !!}

                      

                    </div>

                  </div>

                  @if($i++ == 4) @break @endif

                  @endforeach

                  <!-- end -->



                  

                </div>

              </div>

            </div>

            <!-- Tab panes end -->

            <a href="{{ route('solar_pro') }}" class="butn"><span>View More</span></a>

          </div>

        </div>

      </div>

      <!-- faq ends -->

    </div>

    <!-- Main Container Ends -->

@endsection

@section('page-level-js')

   

    <script>

      new Swiper(".commBan-swiper", {

        speed: 1000,

        autoplay: {

          delay: 5000,

        },

        pagination: {

          el: ".swiper-pagination",

          clickable: true,

        },

      });

      new Swiper(".residential-swiper", {

        slidesPerView: 5,

        slidesPerGroup: 5,

        spaceBetween: 20,

        autoplay: {

          delay: 5000,

        },

        pagination: {

          el: ".residential-pagination",

          clickable: true,

        },

        breakpoints: {

          768: {

            slidesPerView: 4,

            slidesPerGroup: 4,

          },

          576: {

            slidesPerView: 3,

            slidesPerGroup: 3,

            spaceBetween: 0,

          },

        },

      });



      new Swiper(".testimonial-swiper", {

        slidesPerView: 3,

        spaceBetween: 35,

        simulateTouch: false,

        autoplay: {

          delay: 5000,

        },

        navigation: {

          nextEl: ".testimonial-next",

          prevEl: ".testimonial-prev",

        },

        breakpoints: {

          990: {

            slidesPerView: 2,

            spaceBetween: 25,

          },

          576: {

            slidesPerView: 1.5,

            spaceBetween: 15,

          },

          400: {

            slidesPerView: 1.2,

            spaceBetween: 10,

          },

        },

      });



      /* inner testimonial Slider */

      setTimeout(() => {

        for (let index = 1; index <= 4; index++) {

          new Swiper(".innertestimo-swiper-" + index, {

            slidesPerView: 1,

            pagination: {

              el: ".swiper-pagination",

              clickable: true,

            },

          });

        }

      }, 100);



      widthorg = $(window).width();

      if (widthorg <= 990) {

        new Swiper(".client-swiper", {

          slidesPerView: 3,

          spaceBetween: 35,

          autoplay: {

            delay: 5000,

          },

          navigation: {

            nextEl: ".client-next",

            prevEl: ".client-prev",

          },

          breakpoints: {

            768: {

              slidesPerView: 2,

              spaceBetween: 15,

            },

            480: {

              slidesPerView: 1.2,

              spaceBetween: 10,

            },

          },

        });

        $(document).on("click", ".client-box", function () {

          $(".client-back").removeClass("active");

          $(this).next().addClass("active");

        });



        $(document).on("click", ".clientClose", function () {

          $(".client-back").removeClass("active");

        });

      } else {

        $(document).on("click", ".client-box-up", function (e) {

          $(".client-box-up").removeClass("active pre");

          $(this).addClass("active");

          e.stopPropogation();

        });



        $(document).on("click", function () {

          $(".client-box-up").removeClass("active");

          $(".client-box-up").addClass("pre");

        });

        // $(".client-box-up").hover(function () {

        //     $('.client-box-up').removeClass('active pre');

        //     $(this).addClass('active');

        // }, function () {

        //     $('.client-box-up').removeClass('active');

        //     $('.client-box-up').addClass('pre');

        // });

      }

      setTimeout(() => {

        if ($(window).width() > 990) {
                $('.stat-number').counterUp({
                    delay: 10,
                    time: 2000
                });
            }

      }, 100);



    </script>

  @endsection

