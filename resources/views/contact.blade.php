@extends('layout.layout')

  @section('title'){{ $about->meta_title }}@endsection

     @section('seo')

    <!-- Title -->

    <meta itemprop="name" content="{{ $about->meta_title }}" />

    <meta name="twitter:title" content="{{ $about->meta_title }}" />

    <meta property="og:title" content="{{ $about->meta_title }}" />



    <!-- Description -->

    <meta name="description" content="{{ $about->meta_description }}" />

    <meta itemprop="description" content="{{ $about->meta_description }}" />

    <meta name="twitter:description" content="{{ $about->meta_description }}" />

    <meta property="og:description" content="{{ $about->meta_description }}" />



    <!-- keywords -->

    <meta name="keywords" content="{{ $about->meta_keywords }}" />

    <link rel="canonical" href="{{ $about->canonical }}" />
    <meta property="og:url" content="{{ $about->canonical }}" />
    @endsection

     @section('page-level-css')

     <style>

        .mainBan .swiper-wrapper{

            height: 100%;

        }

        .swiper-wrapper{

            height: auto;

        }

    </style>

     @endsection

     @section('tag')

        <h1>{{ $about->meta_title }}</h1>

        @endsection

    @section('content')    

    <!-- go to top -->

    <div class="gotoTop">

      <i class="icon-up-arrow"></i>

    </div>

    <!-- go to top end -->



    <!-- Main Container Starts -->

    <div class="mainContainer">

      @php



        $img = asset('storage/'.str_replace('\\', '/', $about->image));  

      @endphp

      <div

        class="innerBanner"



        style="

          background: url({{ $img }}) no-repeat center center / cover;

        "

      >

        <div

          class="container wow fadeIn"

          data-wow-duration="0.5s"

          data-wow-delay="0s"

        >

          <h2 class="banHdn">{{ $about->title }}</h2>

          <p class="banTitle">

            {{ $about->subtitle }}

          </p>

        </div>

      </div>



            <!-- Meet Our Team Starts -->

            <div class="container">

                <div class="reachBox">

                    <div class="hdn-section wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

                        <h5>Contact Info</h5>

                    </div>



                    <div class="f-row f-3 f-640-2 f-400-1 jufyCen">

                        <div class="f-col wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

                            <!-- start -->

                            <div class="contAddr">

                                <div class="contImg">

                                    <img src="{{ asset('storage/'.json_decode($about->call_img)[0]->download_link) }}" alt="{{ $about->call_alt_tag }}" name="{{ $about->call_image_name }}">

                                </div>

                                <h3>{{ $about->call_text }}</h3>

                                <p><a href="tel:{{ $about->contact }}" target="_blank">{{ $about->contact }}</a></p>

                            </div>

                            <!-- end -->

                        </div>

                        <div class="f-col wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">

                            <!-- start -->

                            <div class="contAddr">

                                <div class="contImg">

                                    <img src="{{ asset('storage/'.json_decode($about->email_img)[0]->download_link) }}" alt="{{ $about->email_alt_tag }}" name="{{ $about->email_image_name }}">

                                </div>

                                <h3>{{ $about->email_text }}</h3>

                                <p><a href="mailto:{{ $about->email }}" target="_blank">{{ $about->email }}</a></p>

                            </div>

                            <!-- end -->

                        </div>

                        <div class="f-col wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.3s">

                            <!-- start -->

                            <div class="contAddr">

                                <div class="contImg">

                                    <img src="{{ asset('storage/'.json_decode($about->address_img)[0]->download_link) }}" alt="{{ $about->address_alt_tag }}" name="{{ $about->address_image_name }}">

                                </div>

                                <h3>{{ $about->address_text }}</h3>

                                <p>{!! $about->address !!}</p>

                            </div>

                            <!-- end -->

                        </div>

                    </div>



                    <!-- start -->

                    <div class="reachFrom wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

                        <div class="hdn-section">

                            <h5>{{ $about->form_headline }}</h5>

                        </div>

                        <form id="enquiryForm">

                            <div class="row">

                                <div class="w33 w-640-50 w-400-100">

                                    <div class="form-group">

                                        <input type="text" id="name" name="name"

                                            oninput="this.value = this.value.replace(/[^A-Za-z\s]/g, '').replace(/(\..*)\./g, '$1');"

                                            class="form-field" />

                                            <p class="form-label">Name</p>

                                    </div>

                                </div>

                                <div class="w33 w-640-50 w-400-100">

                                    <div class="form-group">

                                        <input type="text" id="email" name="email"

                                            class="form-field" />

                                        <p class="form-label">Email ID</p>

                                    </div>

                                </div>

                                <div class="w33 w-640-100">

                                    <div class="form-group">

                                        <input type="text" id="mobile" name="mob_no"

                                            oninput="this.value =

                                            this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                            class="form-field" />

                                        <p class="form-label">Mobile Number</p>

                                    </div>

                                </div>

                                <div class="w100">

                                    <div class="form-textarea-group">

                                        <textarea id="comment" name="comment" class="form-field"></textarea>

                                        <p class="form-label">Comments</p>
                                    </div>

                                </div>



                                <div class="w100 text-center">

                                    <input type="hidden" name="type" value="contact_form">

                                    <button class="butn" type="submit">

                                        <span class="formBtn">Submit</span>

                                    </button>

                                </div>

                                <div class="w100">

                                    <div class="enquiryFormRes alert alert-success" role="alert"></div>

                                </div>



                            </div>

                        </form>

                    </div>

                    <!-- end -->

                </div>

            </div>

            <!-- Meet Our Team Ends -->



        </div>

        <!-- Main Container Ends -->

    @endsection

    @section('page-level-js')

    @endsection



       

