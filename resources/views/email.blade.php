@if($data['type'] == 'residential')
	You have a new Inquiry for residential <br>
	Name : {{ $data['name'] }} , <br>
	Email : {{ $data['email'] }} , <br>
	Contact : {{ $data['mob_no'] }} , <br>
	Pin : {{ $data['pin'] }} , <br>
	Bill : {{ $data['bill'] }} , <br>

	Thanks

@elseif($data['type'] == 'commercial')
	You have a new Inquiry for commercial <br>
	Name : {{ $data['name'] }} , <br>
	Company : {{ $data['c_name'] }} , <br>
	Contact : {{ $data['mob_no'] }} , <br>
	Pin : {{ $data['pin'] }} , <br>
	Bill : {{ $data['bill'] }} , <br>

	Thanks

@elseif($data['type'] == 'contact_form')
	You have a new Inquiry <br>
	Name : {{ $data['name'] }} , <br>
	Email : {{ $data['email'] }} , <br>
	Contact : {{ $data['mob_no'] }} , <br>
	Comment : {{ $data['comment'] }} , <br>

	Thanks

@elseif($data['type'] == 'solar_pro')
	You have a new Inquiry for Solar Pro<br>
	Name : {{ $data['name'] }} , <br>
	Email : {{ $data['email'] }} , <br>
	Contact : {{ $data['mob_no'] }} , <br>
	State : {{ $data['state'] }} , <br>
	City : {{ $data['city'] }} , <br>
	Profession : {{ $data['profession'] }} , <br>

	Thanks
@endif
