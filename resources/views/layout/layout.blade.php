<!doctype html>
<html lang="en" itemscope>

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>@yield('title')</title>
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

        @yield('seo')

        <!-- Image or Logo -->
        <meta name="image" content="{{asset('img/fotlogo.png')}}" />
        <meta itemprop="image" content="{{asset('img/fotlogo.png')}}" />
        <meta name="twitter:image:src" content="{{asset('img/fotlogo.png')}}" />
        <meta property="og:image" content="{{asset('img/fotlogo.png')}}" />

        <!-- URL -->
        

        <meta property="og:site_name" content="Solar Square" />
        <meta property="og:type" content="website" />
        <meta name="twitter:card" content="summary" />

         <!-- Stylesheets -->
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <!-- Google Tag Manager -->
        <script>
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PBS8P6Z');

        </script>
        <!-- End Google Tag Manager -->
        @yield('page-level-css')
    </head>

    <body>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PBS8P6Z" height="0" width="0"
                style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        @yield('tag')

        
        @include('partials.header')

        @yield('content')
        @if(Route::currentRouteName() != 'index')
        @include('partials.footer')
        @endif
         
         @if(Route::currentRouteName() != 'index')
         <script src="{{asset('js/all.js')}}"></script>
         <script>
             function sendForm(form) 
             {

                /* Code Starts */
                var fields = $(form).serializeArray();
                let obj = {
                    'event': 'Lead submission',
                };
                $.each(fields, function (i, field) {
                    // console.log(field);
                    obj[field.name] = field.value;
                });
                console.log(obj);
                // window.dataLayer = window.dataLayer || [];
                // window.dataLayer.push(obj);
                /* Code Ends */


                let btnInitName = $(form).find('.formBtn').html();
                $(form).find('.formBtn').html('Loading..');
                $(form).find('.enquiryFormRes').hide();

                // return; 
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('submit') }}',
                    type: 'POST',
                    data: $(form).serialize(),
                    // dataType: 'html',
                    success: function (response) {
                        console.log(response);

                        // let res = JSON.parse(response);
                        // // console.log(res);
                        // $(form)[0].reset();
                        // $(form).find('.enquiryFormRes').show();
                        // $(form).find('.enquiryFormRes').html(res.message);
                        // $('.form-field').removeClass('field--not-empty');

                        // if ($("#state").length > 0) {
                        //     $('#state').select2('val', ['']);
                        //     $("#profession").select2('val', ['']);

                        //     $('.select2-formgroup').removeClass('has-error');
                        //     $('.select2-formgroup .error').remove();
                        // }


                        // if (res.code == 200) {
                        //     $(form).find('.enquiryFormRes').removeClass('alert-danger');
                        //     $(form).find('.enquiryFormRes').addClass('alert-success');
                        // } else if (res.code == 500) {
                        //     $(form).find('.enquiryFormRes').removeClass('alert-success');
                        //     $(form).find('.enquiryFormRes').addClass('alert-danger');
                        // }
                        // $(form).find('.formBtn').html(btnInitName);

                        // setTimeout(() => {
                        //     $('.enquiryFormRes').hide();
                        // }, 10000);
                    }
                });
            };
         </script>
         <script src="{{asset('js/common.js')}}"></script>
         @else
         <script src="{{asset('js/home.js')}}"></script>
         @endif
        
        @yield('page-level-js')
         </body>

</html>