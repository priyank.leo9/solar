@php

    $why_us = App\SolarEnergy::first();

@endphp

<div class="comm-section p-0 mb-800-50">

                <div class="energyRow padEnergyRow">

                    <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

                        <!-- start -->

                        <div class="solarEng">

                            <img src="{{ asset('storage/'.$why_us->img) }}" alt="{{ $why_us->alt_tag }}" name="{{ $why_us->image_name }}">

                        </div>

                        <!-- end -->

                    </div>

                    <div class="energyRight wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">

                        <!-- start -->

                        <div class="hdn-section left">

                            <h4>{{ $why_us->ques }}</h4>

                            <h5>{{ $why_us->ans }}

                            </h5>

                        </div>

                        <!-- end -->

                        <div class="energyContainer">

                            <!-- start -->

                            @php

                             $qualities = App\Quality::get();   

                            @endphp

                            @foreach($qualities as $quality)

                            <div class="energyBox">

                                <div class="energyImg">

                                    <img src="{{ asset('storage/'.json_decode($quality->img)[0]->download_link) }}" alt="{{ $quality->alt_tag }}" name="{{ $quality->image_name }}" />

                                    <!-- <div class="svgIcon energy1"></div> -->

                                </div>

                                <div class="energyCont">

                                    <h4>{{ $quality->title }}</h4>

                                    <p>{{ $quality->subtitle }}</p>

                                </div>

                            </div>

                            @endforeach

                            <!-- end -->

                            <!-- start -->

                            {{-- <div class="energyBox">

                                <div class="energyImg">

                                    <img src="./img/energy2.svg" alt="img" />

                                    <!-- <div class="svgIcon energy2"></div> -->

                                </div>

                                <div class="energyCont">

                                    <h4>Hassle Free Project Execution</h4>

                                    <p>From design to procurement to installation & govt. approvals</p>

                                </div>

                            </div> --}}

                            <!-- end -->

                            <!-- start -->

                            {{-- <div class="energyBox">

                                <div class="energyImg">

                                    <img src="./img/energy3.svg" alt="img" />

                                    <!-- <div class="svgIcon energy3"></div> -->

                                </div>

                                <div class="energyCont">

                                    <h4>Easy Financing Solutions</h4>

                                    <p>We offer PPA and EMI financing solutions</p>

                                </div>

                            </div> --}}

                            <!-- end -->

                            <!-- start -->

                            {{-- <div class="energyBox">

                                <div class="energyImg">

                                    <img src="./img/energy4.svg" alt="img" />

                                    <!-- <div class="svgIcon energy4"></div> -->

                                </div>

                                <div class="energyCont">

                                    <h4>Only Solar company in India with UL Certification</h4>

                                    <p>UL is the world's toughest industry standard on plant quality and safety

                                    </p>

                                </div>

                            </div> --}}

                            <!-- end -->

                        </div>



                        <!-- <a href="./about-us.html" class="butn"><span>Know More</span></a> -->

                    </div>

                </div>

                <div class="dotIllus">

                    <img src="{{asset('img/dotIllus.svg')}}" alt="Illustration" />

                </div>

            </div>