<div class="comm-section mainForm">

                <div class="container blFormSec">

                    <div class="hdn-section resiComform wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

                        @php

                            $sec_text = App\SectionText::where('section','lead form')->first();        

                        @endphp

                        <h5>{{ $sec_text->title }}</h5>

                        <h4>{{ $sec_text->subtitle }}</h4>

                    </div>

                    <div class="prjtQuery wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">

                        <!-- start -->
                         <!-- start -->
                        @php

                        
                        $route = \Route::currentRouteName(); 

                        @endphp

                        <div class="commTab commTab2">

                            <ul class="nav nav-pills">

                                <li class="nav-item">

                                    <a class="nav-link @if($route == 'residential') active @endif " data-toggle="pill" href="#tab1">Residential</a>

                                </li>

                                <li class="nav-item">

                                    <a class="nav-link @if($route == 'commercial') active @endif" data-toggle="pill" href="#tab2">Commercial</a>

                                </li>

                            </ul>

                        </div>

                        <!-- end -->



                        <!-- Tab panes -->

                        <div class="tab-content">

                            <div class="tab-pane" id="tab1">



                                <!-- start -->

                                <form id="enquiryForm">

                                    <div class="enqboxBtm">

                                        <div class="row">

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="res-name" name="name"

                                                        oninput="this.value = this.value.replace(/[^A-Za-z\s]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Your Name</p>

                                                </div>

                                            </div>

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="res-mobile" name="mob_no"

                                                        oninput="this.value =

                                                    this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Mobile Number</p>

                                                </div>

                                            </div>

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="res-email" name="email"

                                                        class="form-field" />

                                                        <p class="form-label">Email ID</p>

                                                </div>

                                            </div>

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="res-pin" name="pin"

                                                        oninput="this.value =

                                            this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Pin Code</p>

                                                </div>

                                            </div>

                                            <div class="w100">

                                                <div class="form-group">

                                                    <input type="text" id="res-bill" name="bill" oninput="this.value =

                                            this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Enter Average Monthly Electricity Bill</p>

                                                </div>

                                            </div>

                                            <div class="w100">

                                                <input type="hidden" name="type" value="residential">

                                                <button class="butn" type="submit">

                                                    <span class="formBtn">Submit</span>

                                                </button>

                                            </div>

                                            <div class="w100">

                                                <div class="enquiryFormRes alert alert-success" role="alert"></div>

                                            </div>

                                        </div>

                                    </div>

                                </form>

                                <!-- end -->



                            </div>

                            <div class="tab-pane active" id="tab2">



                                <!-- start -->

                                <form id="enquiryForm1">

                                    <div class="enqboxBtm">

                                        <div class="row">

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="com-name" name="name"

                                                        oninput="this.value = this.value.replace(/[^A-Za-z\s]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Your Name</p>

                                                </div>

                                            </div>

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="com_c_name" name="c_name"
                                                        
                                                        class="form-field" />

                                                        <p class="form-label">Company Name</p>

                                                </div>

                                            </div>

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="com-mobile" name="mob_no"

                                                    oninput="this.value =

                                                    this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Mobile Number</p>

                                                </div>

                                            </div>

                                            <div class="w50 w-480-100">

                                                <div class="form-group">

                                                    <input type="text" id="com-pin" name="pin"

                                                        oninput="this.value =

                                            this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Pin Code</p>

                                                </div>

                                            </div>

                                            <div class="w100">

                                                <div class="form-group">

                                                    <input type="text" id="com-bill" name="bill" oninput="this.value =

                                            this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"

                                                        class="form-field" />

                                                        <p class="form-label">Enter Average Monthly Electricity Bill</p>

                                                </div>

                                            </div>



                                            <div class="w100">

                                                <input type="hidden" name="type" value="commercial">

                                                <button class="butn" type="submit">

                                                    <span class="formBtn">Submit</span>

                                                </button>

                                            </div>

                                            <div class="w100">

                                                <div class="enquiryFormRes alert alert-success" role="alert"></div>

                                            </div>

                                        </div>

                                    </div>

                                </form>

                                <!-- end -->

                            </div>

                        </div>



                    </div>



                </div>

            </div>