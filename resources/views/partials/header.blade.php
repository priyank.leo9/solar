

<header class="@if(\Route::currentRouteName()=='index') homeHeader @else innerheader @endif" id="header">

<div class="container">

    <div class="header-box">

        <a class="logo" href="{{ route('index') }}">

            <img class="defLogo" src="{{asset('img/logo.png')}}" alt="solar square">

            <img class="srlLogo" src="{{asset('img/fotlogo.png')}}" alt="solar square">

        </a>

        <ul class="header-rig">

            <li><a href="{{ route('commercial') }}" class="commercial-link @if(\Route::currentRouteName()=='commercial') active @endif">Commercial</a></li>

            <li><a href="{{ route('residential') }}" class="residential-link @if(\Route::currentRouteName()=='residential') active @endif">Residential</a></li>

            <li><a href="{{ route('about') }}" class="about-link @if(\Route::currentRouteName()=='about') active @endif">About Us</a></li>

            <li><a href="{{ route('contact') }}" class="contact-link @if(\Route::currentRouteName()=='contact') active @endif">Contact Us</a></li>

            <li class="solBtn"><a href="{{ route('solar') }}" class="butn btn-blue"><span>Go Solar</span></a></li>

            <li class="menu">

                <div class="side-menu"><i class="icon-menu"></i></div>

            </li>

        </ul>

    </div>

</div>





<!-- Side Menu -->

<div class="overlay"></div>

<div class="mobile-menu">

    <div class="close-btn">

        <i class="icon-cross"></i>

    </div>

    <ul class="header-rig">

        <li><a href="{{ route('commercial') }}">Commercial</a></li>

        <li><a href="{{ route('residential') }}">Residential</a></li>

        <li><a href="{{ route('about') }}">About Us</a></li>

        <li><a href="{{ route('contact') }}">Contact Us</a></li>

        <!-- <li><a href="#" class="butn btn-blue">Go Solar</a></li> -->

    </ul>

</div>

</header>

<!-- Side Menu End -->

