@php

    $creds = App\Credential::get();

    $clients = App\Client::get();

    $sec_text = App\SectionText::where('section','credentials')->first();    

@endphp

<div class="comm-section pb-4">

                <div class="container">

                    <!-- start -->

                    <div class="hdn-section wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">

                        <h4>{{ $sec_text->title }}</h4>

                        <h5>{{ $sec_text->subtitle }}</h5>

                    </div>

                    <!-- end -->

                    <!-- start -->

                    <div class="crd-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">

                        @foreach( $creds as $cred )

                        <div class="crd-list">

                            <div class="crd-icon-box">

                                <img src="{{asset('storage/'.json_decode($cred->img)[0]->download_link) }} " alt="{{ $cred->alt_tag }}" name="{{ $cred->image_name }}">

                            </div>

                            <div class="crd-icon-cont">

                                <h5>{{ $cred->prfix_text }}<span class="stat-number">{{ $cred->count }}</span>{{ $cred->supporting_text }}</h5>

                                <p>{{ $cred->title }}</p>

                            </div>

                        </div>

                        @endforeach

                       

                    </div>

                    <!-- end -->

                    <!-- Swiper Starts -->

                    <div class="swiper-container residential-swiper wow fadeIn" data-wow-duration="0.5s"

                        data-wow-delay="0.2s">

                        <!-- Additional required wrapper -->

                        <div class="swiper-wrapper">

                            <!-- Slides -->

                            @foreach($clients as $client)

                            <div class="swiper-slide">

                                <div class="residential-logo"><img src="{{ asset('storage/'.$client->img) }}" alt="{{ $client->alt_tag }}" name="{{ $client->image_name }}" /></div>

                            </div>

                            @endforeach

                            <!-- Slides -->

                        </div>

                        <div class="swiper-pagination residential-pagination"></div>

                    </div>

                    <!-- Swiper ends -->

                    <!--   <div class="w-100 text-center mt-5 wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">

                        <a href="./commercial-work.html" class="butn"><span>View project</span></a>

                    </div> -->

                </div>

            </div>