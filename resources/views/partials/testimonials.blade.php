@php
    $testimonials = App\Testimonial::get();  
    $sec_text = App\SectionText::where('section','testimonials')->first();  
@endphp
<div class="comm-section">
                <div class="container">
                    <!-- start -->
                    <div class="hdn-section wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <h4>{{ $sec_text->title }}</h4>
                        <h5>{{ $sec_text->subtitle }}</h5>
                    </div>
                    <!-- end -->
                    <!-- Swiper Starts -->
                    <div class="swiper-container testimonial-swiper wow fadeIn" data-wow-duration="0.5s"
                        data-wow-delay="0.2s">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Johnson & Johnson -->
                            @foreach( $testimonials as $testimonial )
                            <div class="swiper-slide swiperAuto">
                                <div class="testim-box">
                                    <div class="swiper-container innertestimo-swiper innertestimo-swiper-2">
                                        @php
                                            $images = json_decode($testimonial->image);    
                                        @endphp
                                        <div class="swiper-wrapper">
                                            @foreach( $images as $img )
                                            <div class="swiper-slide">
                                                <div class="testimoImg">
                                                    <img src="{{ asset('storage/'.$img) }}" >
                                                </div>
                                            </div>
                                            @endforeach
                                            
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>

                                    <div class="testim-cont">
                                        <div>
                                            <h4>{{ $testimonial->c_name }}</h4>
                                            <p>{{ $testimonial->text }}</p>
                                        </div>
                                        <div class="testim-user">
                                            <div class="testim-user-img">
                                                <img src="{{ asset('storage/'.$testimonial->logo) }}" alt="{{ $testimonial->alt_tag }}" name="{{ $testimonial->image_name }}" />
                                            </div>
                                            <div class="testim-user-cont">
                                                <h5>{{ $testimonial->p_name }}</h5>
                                                <h6>{{ $testimonial->designation }}</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                           
                        </div>
                        <div class="testimonial-btn">
                            <div class="swiper-button-prev testimonial-prev"></div>
                            <div class="swiper-button-next testimonial-next"></div>
                        </div>
                    </div>
                    <!-- Swiper ends -->
                </div>
            </div>