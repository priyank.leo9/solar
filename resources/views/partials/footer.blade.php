<footer id="footer" class="wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
<div class="footer">
  <div class="container">
    <div class="fotTop">
      @php
        $contact = App\ContactInfo::first(); 
        $sec_text = App\SectionText::where('section','social media')->first();  
      @endphp
      <p>{{ $sec_text->title }}</p>
      <h6>{{ $sec_text->subtitle }}</h6>
      <ul class="social-icon">
        <li>
          <a href="{{ $contact->fb_link }}" target="_blank"
            ><i class="icon-fb"></i
          ></a>
        </li>
        <li>
          <a
            href="{{ $contact->linked_in_link }}"
            target="_blank"
            ><i class="icon-linkedin"></i
          ></a>
        </li>
        <li>
          <a href="{{ $contact->instagram_link }}" target="_blank"
            ><i class="icon-insta"></i
          ></a>
        </li>
      </ul>
    </div>

    <div class="fotMid">
      <div>
        <div class="fotlogo">
          <img src="{{asset('img/fotlogo.png')}}" alt="solar square" />
        </div>
      </div>

      <div class="fot-links">
        <p>Projects</p>
        <ul>
          <li>
            <a href="{{ route('residential') }}">Homes & Residential Societies</a>
          </li>
          <li><a href="{{ route('commercial') }}">Commercial, Industrial & SMEs</a></li>
        </ul>
      </div>

      <div class="fot-links">
        <p>Quick Links</p>
        <ul>
          <li><a href="{{ route('about') }}">About Us</a></li>
          <li><a href="{{ route('contact') }}">Contact Us</a></li>
          <li><a href="{{ route('solar_pro') }}">Solar Pro</a></li>
        </ul>
      </div>

      <div class="fot-contact">
        <ul>
          <li>
            <a href="{{ route('solar') }}">
              <i class="icon-solar"></i>
              <span>Go Solar</span>
            </a>
          </li>
          <li>
            <a href="tel:{{ $contact->contact }}">
              <i class="icon-mobile"></i>
              <span>{{ $contact->contact }}</span>
            </a>
          </li>
          <li>
            <a href="mailto:{{ $contact->email }}">
              <i class="icon-mail"></i>
              <span>{{ $contact->email }}</span>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="fotcopy">
      <p>Copyright © 2020 Solar Square. All rights reserved.</p>
    </div>
  </div>
</div>
</footer>
