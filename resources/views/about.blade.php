@extends('layout.layout')
  @section('title'){{ $about->meta_title }}@endsection
     @section('seo')
    <!-- Title -->
    <meta itemprop="name" content="{{ $about->meta_title }}" />
    <meta name="twitter:title" content="{{ $about->meta_title }}" />
    <meta property="og:title" content="{{ $about->meta_title }}" />

    <!-- Description -->
    <meta name="description" content="{{ $about->meta_description }}" />
    <meta itemprop="description" content="{{ $about->meta_description }}" />
    <meta name="twitter:description" content="{{ $about->meta_description }}" />
    <meta property="og:description" content="{{ $about->meta_description }}" />

    <!-- keywords -->
    <meta name="keywords" content="{{ $about->meta_keywords }}" />

    <link rel="canonical" href="{{ $about->canonical }}" />
        <meta property="og:url" content="{{ $about->canonical }}" />
    @endsection
     @section('page-level-css')
     <style>
        .mainBan .swiper-wrapper{
            height: 100%;
        }
        .swiper-wrapper{
            height: auto;
        }
    </style>
     @endsection
     @section('tag')
        <h1>{{ $about->meta_title }}</h1>
        @endsection
    @section('content')    
    <!-- go to top -->
    <div class="gotoTop">
      <i class="icon-up-arrow"></i>
    </div>
    <!-- go to top end -->

    <!-- Main Container Starts -->
    <div class="mainContainer">
      @php

        $img = asset('storage/'.str_replace('\\', '/', $about->image));  
      @endphp
      <div
        class="innerBanner"

        style="
          background: url({{ $img }}) no-repeat center center / cover;
        "
      >
        <div
          class="container wow fadeIn"
          data-wow-duration="0.5s"
          data-wow-delay="0s"
        >
          <h2 class="banHdn">{{ $about->title }}</h2>
          <p class="banTitle">
            {{ $about->subtitle }}
          </p>
        </div>
      </div>
      @php
        $sec_text = App\SectionText::where('section','vision')->first();    
        $sec_text1 = App\SectionText::where('section','team')->first();    
      @endphp
      <!-- Our Vision Starts -->
      <div class="comm-section partner-sec-6-container energyBox3">
        <div class="container">
          <!-- start -->
          <div
            class="hdn-section wow fadeIn"
            data-wow-duration="0.5s"
            data-wow-delay="0.1s"
          >
            <h4>Our Vision</h4>
            <h5>{{ $sec_text->title }}</h5>
            <h6>
              {{ $sec_text->subtitle }}
            </h6>
          </div>
          <div class="energyContainer">
            <!-- start -->
            @foreach( $visions as $vision )
            <div
              class="energyBox wow fadeInUp"
              data-wow-duration="0.5s"
              data-wow-delay="0.1s"
            >
              <div class="energyImg">
                <img src="{{ asset('storage/'.json_decode($vision->image)[0]->download_link) }}" alt="{{ $vision->alt_tag }}" name="{{ $vision->image_name }}" />
              </div>
              <div class="energyCont">
                <h4>
                  {{ $vision->text }}
                </h4>
              </div>
            </div>
            <!-- end -->
            @endforeach
            <!-- start -->
            
          </div>
        </div>
      </div>
      <!-- Our Vision Ends -->
      <!-- Meet Our Team Starts -->
      <div class="comm-section gryGradient pb-0">
        <div class="container teamContainer">
          <!-- start -->
          <div
            class="hdn-section wow fadeIn"
            data-wow-duration="0.5s"
            data-wow-delay="0.1s"
          >
            <h5>{{ $sec_text1->title }}</h5>
          </div>
          <div class="f-row f-3 f-640-2 jufyCen">
            @foreach( $teams as $team )
            <!-- start -->
            <div
              class="f-col wow fadeIn"
              data-wow-duration="0.5s"
              data-wow-delay="0.1s"
            >
              <div class="teamBox">
                <div class="teamImg">
                  <img src="{{ asset('storage/'.$team->image) }}" alt="{{ $team->alt_tag }}" name="{{ $team->image_name }}" />
                </div>
                <div class="teamCont">
                  <h6>{{ $team->name }}</h6>
                  <p>{{ $team->designation }}</p>
                </div>
              </div>
            </div>
            <!-- end -->
            @endforeach
            <!-- start -->
           
          </div>
        </div>
      </div>
      <!-- Meet Our Team Ends -->
      <!-- Customer Testimonials Starts -->
      @include('partials.testimonials')
      <!-- Customer Testimonials Ends -->
    </div>
    <!-- Main Container Ends -->
@endsection
@section('page-level-js')
   
    <script>
      $("#commentForm").validate();

      new Swiper(".testimonial-swiper", {
        slidesPerView: 3,
        spaceBetween: 35,
        simulateTouch: false,
        autoplay: {
          delay: 5000,
        },
        navigation: {
          nextEl: ".testimonial-next",
          prevEl: ".testimonial-prev",
        },
        breakpoints: {
          990: {
            slidesPerView: 2,
            spaceBetween: 25,
          },
          576: {
            slidesPerView: 1.5,
            spaceBetween: 15,
          },
          400: {
            slidesPerView: 1.2,
            spaceBetween: 10,
          },
        },
      });
      /* inner testimonial Slider */
      setTimeout(() => {
        for (let index = 1; index <= 4; index++) {
          new Swiper(".innertestimo-swiper-" + index, {
            slidesPerView: 1,
            pagination: {
              el: ".swiper-pagination",
              clickable: true,
            },
          });
        }
      }, 100);
    </script>
 @endsection
