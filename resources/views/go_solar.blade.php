@extends('layout.layout')
  @section('title'){{ $about->meta_title }}@endsection
     @section('seo')
    <!-- Title -->
    <meta itemprop="name" content="{{ $about->meta_title }}" />
    <meta name="twitter:title" content="{{ $about->meta_title }}" />
    <meta property="og:title" content="{{ $about->meta_title }}" />

    <!-- Description -->
    <meta name="description" content="{{ $about->meta_description }}" />
    <meta itemprop="description" content="{{ $about->meta_description }}" />
    <meta name="twitter:description" content="{{ $about->meta_description }}" />
    <meta property="og:description" content="{{ $about->meta_description }}" />

    <!-- keywords -->
    <meta name="keywords" content="{{ $about->meta_keywords }}" />

    <link rel="canonical" href="{{ $about->canonical }}" />
    <meta property="og:url" content="{{ $about->canonical }}" />
    @endsection
     @section('page-level-css')
     <style>
        .mainBan .swiper-wrapper{
            height: 100%;
        }
        .swiper-wrapper{
            height: auto;
        }
    </style>
     @endsection
     @section('tag')
        <h1>{{ $about->meta_title }}</h1>
        @endsection
    @section('content')    
    <!-- go to top -->
    <div class="gotoTop">
      <i class="icon-up-arrow"></i>
    </div>
    <!-- go to top end -->

        <!-- Main Container Starts -->
        <div class="mainContainer">

            <!-- Bnaner start -->
            <div class="mainBan">
                <!-- start -->
                <div class="banImg leftBan">
                    <img src="{{ asset('storage/'.$about->image) }}" alt="{{ $about->alt_tag }}" name="{{ $about->image_name }}" />
                    <div class="container">
                        <div class="commCont wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                            <h2 class="banHdn">{{ $about->title_line1 }}<br />{{ $about->title_line2 }}</h2>
                        </div>
                    </div>
                </div>
                <!-- end -->
            </div>
            <!-- Bnaner end -->
            <!-- form start -->
            @include('partials.lead')
            <!-- form end -->
            <!-- faq start -->
            <div class="comm-section pb-0">

                <div class="hdn-section wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                    <h5>{{ $faq->heading }}</h5>
                </div>

                <!-- FAQ 1 Starts -->
                <div class="energyRow nobg revs--energyRow">
                    <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <!-- start -->
                        <div class="solarEng">
                            <img src="{{ asset('storage/'.$faq->slider_1) }}" alt="{{ $faq->alt_tag1 }}" name="{{ $faq->image_name }}">
                        </div>
                        <!-- end -->
                    </div>
                    <div class="energyRight faqRight wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <!-- start -->
                        <div class="hdn-section left">
                            <h6 class="bluCol">{{ $faq->type1_text }}</h6>
                        </div>
                        <div class="faqacc-box">
                             @foreach( $ones as $one )
                              <div class="faqAcc">
                                <div class="faqQus">
                                  <h4>{{ $one->ques }}</h4>
                                </div>
                                <div class="faqpara">
                                  <p>
                                    {!! $one->ans !!}
                                  </p>
                                </div>
                              </div>
                              <!-- end -->
                             @endforeach
                            <!-- end -->

                        </div>
                        <!-- end -->
                    </div>
                </div>
                <!-- FAQ 1 Ends -->

                <!-- FAQ 2 Starts -->
                <div class="energyRow nobg">
                    <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <div class="solarEng">
                            <img src="{{ asset('storage/'.$faq->slider_2) }}" alt="{{ $faq->alt_tag2 }}" name="{{ $faq->image_name2 }}">
                        </div>
                    </div>
                    <div class="energyRight faqRight wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <div class="hdn-section left">
                            <h6 class="bluCol">{{ $faq->type2_text }}</h6>
                        </div>
                        <div class="faqacc-box">
                            @foreach( $twos as $two )
                              <!-- start -->
                              <div class="faqAcc">
                                <div class="faqQus">
                                  <h4>{{ $two->ques }}</h4>
                                </div>
                                <div class="faqpara">
                                  <p>
                                    {!! $two->ans !!}
                                  </p>
                                </div>
                              </div>
                              <!-- end -->
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- FAQ 2 Ends -->

                <!-- FAQ 3 Starts -->
                <div class="energyRow nobg revs--energyRow">
                    <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <div class="solarEng">
                            <img src="{{ asset('storage/'.$faq->slider_3) }}" alt="{{ $faq->alt_tag3 }}" name="{{ $faq->image_name3 }}">
                        </div>
                    </div>
                    <div class="energyRight faqRight wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <div class="hdn-section left">
                            <h6 class="bluCol">{{ $faq->type3_text }}</h6>
                        </div>
                        <div class="faqacc-box">
                            @foreach( $threes as $three )
                              <div class="faqAcc">
                                <div class="faqQus">
                                  <h4>{{ $three->ques }}</h4>
                                </div>
                                <div class="faqpara">
                                  <p>
                                    {!! $three->ans !!}
                                  </p>
                                </div>
                              </div>
                            @endforeach
                                <!-- end -->

                        </div>
                        <!-- end -->
                    </div>
                </div>
                <!-- FAQ 3 Ends -->

            </div>
            <!-- faq ends -->


        </div>
        <!-- Main Container Ends -->
@endsection
@section('page-level-js')
@endsection

