@extends('layout.layout')
       @section('title'){{ $commercial->meta_title }}@endsection
        @section('seo')
        <!-- Title -->
        <meta itemprop="name" content="{{ $commercial->meta_title }}" />
        <meta name="twitter:title" content="{{ $commercial->meta_title }}" />
        <meta property="og:title" content="{{ $commercial->meta_title }}" />

        <!-- Description -->
        <meta name="description" content="{{ $commercial->meta_description }}" />
        <meta itemprop="description" content="{{ $commercial->meta_description }}" />
        <meta name="twitter:description" content="{{ $commercial->meta_description }}" />
        <meta property="og:description" content="{{ $commercial->meta_description }}" />

        <!-- keywords -->
        <meta name="keywords" content="{{ $commercial->meta_keywords }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        
        <link rel="canonical" href="{{ $commercial->canonical }}" />
        <meta property="og:url" content="{{ $commercial->canonical }}" />
        @endsection
    @section('page-level-css')
    
    <style>
        .mainBan .swiper-wrapper{
            height: 100%;
        }
        .swiper-wrapper{
            height: auto;
        }
    </style>
    @endsection

        @section('tag')
        <h1>{{ $commercial->meta_title }}</h1>
        @endsection
    @section('content')    
        <!-- go to top -->
        <div class="gotoTop">
            <i class="icon-up-arrow"></i>
        </div>
        <!-- go to top end -->

        <!-- Main Container Starts -->
        <div class="mainContainer">

            <!-- Bnaner start -->
             <div class="swiper-container mainBan commBan-swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <div class="banImg leftBan">
                            <img src="{{ asset('storage/'.$commercial->slider_1) }}" alt="{{ $commercial->alt_tag1 }}" name="{{ $commercial->image_name }}">
                            <!-- start -->
                            <div class="container">
                                <div class="commCont wow fadeIn" data-wow-duration="0.5s">
                                    <h2 class="banHdn">{{ $commercial->title1 }}<br />
                                        {{ $commercial->title1_line2 }}</h2>
                                    <p class="banTitle">{{ $commercial->subtitle1 }}</p>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="banImg leftBan">
                            <img src="{{ asset('storage/'.$commercial->slider_2) }}" alt="{{ $commercial->alt_tag2 }}" name="{{ $commercial->image_name2 }}">
                            <!-- start -->
                            <div class="container">
                                <div class="commCont wow fadeIn" data-wow-duration="0.5s">
                                    <h2 class="banHdn">{{ $commercial->title2 }}<br />
                                        {{ $commercial->title2_line2 }}</h2>
                                    <p class="banTitle">{{ $commercial->subtitle2 }}</p>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="banImg leftBan">
                            <img src="{{asset('storage/'.$commercial->slider_3)}}" alt="{{ $commercial->alt_tag3 }}" name="{{ $commercial->image_name3 }}">
                            <!-- start -->
                            <div class="container">
                                <div class="commCont wow fadeIn" data-wow-duration="0.5s">
                                    <h2 class="banHdn">{{ $commercial->title3 }}<br />
                                        {{ $commercial->title3_line2 }}</h2>
                                    <p class="banTitle">{{ $commercial->subtitle3 }}</p>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination banPagi"></div>
            </div>
            <!-- Bnaner end -->

            <!-- Client Starts -->
            <div class="partner-sec-1-container">
                <div class="container">
                    <div class="row">
                        <div class="w50 w-768-100">
                            <div class="partner-sec-1-left wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                                <!-- start -->
                                <div class="hdn-section left">
                                    <h5>{{ $commercial->whom_to_sell_text }}</h5>
                                </div>
                                <!-- end -->
                                <ul>
                                    @foreach( $sells as $sell )
                                    <li>
                                        <img src="{{ asset('storage/'.json_decode($sell->image)[0]->download_link) }}" alt="{{ $sell->alt_tag }}" name="{{ $sell->image_name }}" />
                                        <span>{{ $sell->text }}</span>
                                    </li>
                                    @endforeach
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="w50 w-768-100">
                            <div class="partner-sec-1-right wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                                <h4>{{ $commercial->form_headline }}</h4>
                                <form id="enquiryForm">
                                    <div class="partner-form">
                                        <div class="row">
                                            <div class="w100 w-768-50 w-480-100">
                                                <div class="form-group">
                                                    <input type="text" id="name" name="name" class="form-field"
                                                        placeholder="Full Name" oninput="this.value =
                                                        this.value.replace(/[^A-Za-z\s]/g, '').replace(/(\..*)\./g,
                                                        '$1');" />
                                                </div>
                                            </div>
                                            <div class="w100 w-768-50 w-480-100">
                                                <div class="form-group">
                                                    <input type="text" id="email" name="email" class="form-field"
                                                        placeholder="Email" />
                                                </div>
                                            </div>
                                            <div class="w100 w-768-50 w-480-100">
                                                <div class="form-group">
                                                    <input type="text" id="mobile" name="mob_no" class="form-field"
                                                        oninput="this.value =
                                            this.value.replace(/[^0-9+-]/g, '').replace(/(\..*)\./g, '$1');"
                                                        class="form-field" placeholder="Mobile" />
                                                </div>
                                            </div>
                                            <div class="w100 w-768-50 w-480-100">
                                                <div class="form-group select2-formgroup">
                                                    <select id="state" name="state" class="select-field form-field">
                                                        <option></option>
                                                        @foreach( $states as $state )
                                                        <option value="{{ $state->state }}">{{ $state->state }}</option>
                                                        @endforeach
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w100 w-768-50 w-480-100">
                                                <div class="form-group">
                                                    <input type="text" id="city" oninput="this.value = this.value.replace(/[^A-Za-z\s]/g, '').replace(/(\..*)\./g,
                                                        '$1');" name="city" class="form-field"
                                                        placeholder="City" />
                                                </div>
                                            </div>
                                            <div class="w100 w-768-50 w-480-100">
                                                <div class="form-group select2-formgroup">
                                                    <select id="profession" name="profession"
                                                        class="select-field form-field">
                                                        <option disabled selected></option>
                                                        @foreach( $professions as $profession )
                                                        <option value="{{ $profession->profession }}">{{ $profession->profession }}</option>
                                                        @endforeach
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w100 text-center">
                                                <input type="hidden" name="type" value="solar_pro">
                                                <button class="butn" type="submit">
                                                    <span class="formBtn">Partner With Us</span>
                                                </button>
                                            </div>
                                            <div class="w100 text-center">
                                                <div class="enquiryFormRes alert alert-success" role="alert"></div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Client Ends -->
            <!-- <div class="mb-50 mb-1440-30 mb-990-0"></div> -->
            <!-- Credentials Starts -->
            @include('partials.credentials')
            <!-- Credentials Ends -->
            <!-- <div class="mb-100  mb-1440-50 mb-990-20"></div> -->

            <!-- Why chose SolarSquare? Starts -->
           @include('partials.why_choose_us')
            <!-- Why chose SolarSquare? Ends -->

            <!-- <div class="mb-100  mb-1440-50 mb-990-20"></div> -->
            <!-- What do you get by being a Solar Sales Partner? Starts -->
            @php
                $sec_text = App\SectionText::where('section','benefits')->first();
            @endphp
            <div class="partner-sec-4-container pb-0">
                <div class="container">
                    <!-- start -->
                    <div class="hdn-section wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <h5>{{ $sec_text->title }}</h5>
                    </div>
                    <div class="energyContainer wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s"> 
                        @foreach( $benefits as $benefit )
                        <!-- start -->
                        <div class="energyBox">
                            <div class="energyImg"><img src="{{ asset('storage/'.json_decode($benefit->image)[0]->download_link) }}" alt="{{ $benefit->alt_tag }}" name="{{ $benefit->image_name }}" /></div>
                            <div class="energyCont">
                                <h4>{{ $benefit->title }}</h4>
                                <p>{{ $benefit->subtitle }}
                                </p>
                            </div>
                        </div>
                        <!-- end -->
                        @endforeach
                       
                    </div>
                </div>
            </div>
            <!-- What do you get by being a Solar Sales Partner? Ends -->
            <!-- <div class="mb-100  mb-1440-50 mb-990-20"></div> -->


            <!-- How It Works Starts Starts -->
            <div class="comm-section partner-sec-5-container pb-0">
                <div class="energyRow energyRow-2">
                    <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <!-- start -->
                        <div class="solarEng">
                            <img src="{{ asset('storage/'.$commercial->how_it_work_img) }}" alt="{{ $commercial->work_alt_tag }}" name="{{ $commercial->work_image_name }}">
                        </div>
                        <!-- end -->
                    </div>
                    <div class="energyRight wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <!-- start -->
                        <div class="hdn-section left">
                            <h4>{{ $commercial->work_title }}</h4>
                            <h5>{{ $commercial->work_subtitle }}<br /> {{ $commercial->work_subtitle_line2 }}</h5>
                        </div>
                        <!-- end -->
                        <div class="energyContainer">
                            @foreach($works as $work)
                            <!-- start -->
                            <div class="energyBox">
                                <div class="energyImg"><img src="{{ asset('storage/'.json_decode($work->image)[0]->download_link) }}" alt="{{ $work->alt_tag }}" name="{{ $work->image_name }}" /></div>
                                <div class="energyCont">
                                    <h4>{{ $work->title }}</h4>
                                    <p>{{ $work->subtitle }} </p>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
                <div class="dotIllus">
                    <img src="{{asset('img/dotIllus.svg')}}" alt="Illustration" />
                </div>
            </div>
            <!-- How It Works Starts Ends -->

            <!-- <div class="mb-100  mb-1440-50 mb-990-20"></div> -->
            <!-- Partner with us Starts -->
            @php
                $sec_text1 = App\SectionText::where('section','partner')->first();
            @endphp
            <div class="comm-section partner-sec-6-container energyBox4 pb-0">
                <div class="container">
                    <!-- start -->
                    <div class="hdn-section wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <h4>{{ $sec_text1->title }}</h4>
                        <h5>{{ $sec_text1->subtitle }}</h5>
                    </div>
                    <div class="energyContainer wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        @foreach( $partners as $partner )
                        <!-- start -->
                        <div class="energyBox">
                            <div class="energyImg"><img src="{{ asset('storage/'.json_decode($partner->image)[0]->download_link) }}" alt="{{ $partner->alt_tag }}" name="{{ $partner->image_name }}" /></div>
                            <div class="energyCont">
                                <h4>{{ $partner->title }}</h4>
                            </div>
                        </div>
                        <!-- end -->
                        @endforeach
                      
                    </div>
                </div>
            </div>
            <!-- Partner with us Ends -->

            <!-- <div class="mb-100  mb-1440-50 mb-990-20"></div> -->

            <!-- faq start -->
            <div class="comm-section pb-0">

                <div class="energyRow revs--energyRow">
                    <div class="energyLeft wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <!-- start -->
                        <div class="solarEng">
                            <img src="{{ asset('storage/'.$faq->image) }}" alt="{{ $faq->alt_tag }}" name="{{ $faq->img_name }}">
                        </div>
                        <!-- end -->
                    </div>
                    <div class="energyRight wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <!-- start -->
                        <div class="hdn-section left">
                            <h5>{{ $faq->heading }}</h5>
                        </div>
                        <!-- end -->

                        <div class="commTab commTab2">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#faq1">{{ $faq->type1_text }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#faq2">{{ $faq->type2_text }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#faq3">{{ $faq->type3_text }}</a>
                                </li>
                            </ul>
                        </div>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="faq1">
                                <div class="faqacc-box">
                                    <!-- start -->
                                    @foreach( $ones as $one )
                                    <div class="faqAcc">
                                        <div class="faqQus">
                                            <h4> {{ $one->ques }}</h4>
                                        </div>
                                        <div class="faqpara">
                                            {!! $one->ans !!}
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- end -->

                                   

                                </div>
                            </div>

                            <div class="tab-pane" id="faq2">
                                <div class="faqacc-box">
                                    <!-- start -->
                                    @foreach( $twos as $two )
                                    <div class="faqAcc">
                                        <div class="faqQus">
                                            <h4>{{ $two->ques }}</h4>
                                        </div>
                                        <div class="faqpara">
                                            {!! $two->ans !!}
                                        </div>
                                    </div>
                                   @endforeach

                                </div>
                            </div>

                            <div class="tab-pane" id="faq3">
                                <div class="faqacc-box">
                                    <!-- start -->
                                    @foreach( $threes as $three )
                                    <div class="faqAcc">
                                        <div class="faqQus">
                                            <h4>{{ $three->ques }}</h4>
                                        </div>
                                        <div class="faqpara">
                                            {!! $three->ans !!}
                                        </div>
                                    </div>
                                    @endforeach
                                   
                                </div>
                            </div>
                        </div>
                        <!-- Tab panes end -->

                    </div>
                </div>

            </div>
            <!-- faq ends -->
            <!-- <div class="mb-100  mb-1440-50 mb-990-20"></div> -->


        </div>
        <!-- Main Container Ends -->
@endsection
@section('page-level-js')

        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      
        <script>
            $("#state").select2({
                placeholder: "State",
            }).on('change', function () {
                $(this).valid();
            });

            $("#profession").select2({
                placeholder: "Profession",
            }).on('change', function () {
                $(this).valid();
            });


            new Swiper('.commBan-swiper', {
                speed: 1000,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });

            new Swiper('.residential-swiper', {
                slidesPerView: 5,
                slidesPerGroup: 5,
                spaceBetween: 20,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.residential-pagination',
                    clickable: true,
                },
                breakpoints: {
                    768: {
                        slidesPerView: 4,
                        slidesPerGroup: 4,
                    },
                    576: {
                        slidesPerView: 3,
                        slidesPerGroup: 3,
                        spaceBetween: 0,
                    },
                }
            });

            if ($(window).width() > 990) {
                $('.stat-number').counterUp({
                    delay: 10,
                    time: 2000
                });
            }

        </script>
   @endsection
